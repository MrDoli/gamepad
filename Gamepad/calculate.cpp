#include "calculate.h"
#include <QDebug>
#include <math.h>

#define FIRST_CONST_VALUE 0.2
#define SECOND_CONST_VALUE -0.2
#define CONST_TAB 2

/*!
 * @brief Calculate::Calculate
 * Konstruktor klasy Calculate, przydzielanie pamięci steering
 */
Calculate::Calculate()
{
    this->steering=new Steering();
}

/*!
 * \brief Calculate::makeDesision
 * Podejmowanie decyzji w którym z 8 obszarów znajduje się drążek
 * \param x wartość na osi OX wychylenia drążka
 * \param y wartość na osi OY wychylenia drążka
 */
void Calculate::makeDesision(double x, double y)
{
    if(x>=SECOND_CONST_VALUE && x<=FIRST_CONST_VALUE && y<=FIRST_CONST_VALUE && y>=SECOND_CONST_VALUE)
        this->inFirstArea();
    if(x<SECOND_CONST_VALUE && y>SECOND_CONST_VALUE && y<FIRST_CONST_VALUE)
        this->inSecondArea(x);
    if(x>FIRST_CONST_VALUE && y>SECOND_CONST_VALUE && y<FIRST_CONST_VALUE)
        this->inThirdArea(x);
    if(x<=SECOND_CONST_VALUE && y>= FIRST_CONST_VALUE)  //UWAGA
        this->inFourthArea(x,y);
    if(x>SECOND_CONST_VALUE && x<FIRST_CONST_VALUE && y>FIRST_CONST_VALUE)
        this->inFifthArea(y);
    if(x>=FIRST_CONST_VALUE && y>=FIRST_CONST_VALUE)
        this->inSixthArea(x,y);
    if(x<=SECOND_CONST_VALUE && y<= SECOND_CONST_VALUE)
        this->inSevenArea(x,y);
    if(x>SECOND_CONST_VALUE && x<FIRST_CONST_VALUE && y<=SECOND_CONST_VALUE)
        this->inEighthArea(y);
    if(x>=FIRST_CONST_VALUE && y<=SECOND_CONST_VALUE)
        this->inNinthArea(x,y);
}

void Calculate::inFirstArea()
{
   steering->setEngineL(0);
   steering->setEngineP(0);
   this->setDirectTab(0,1);
   this->setDirectTab(1,1);
   steering->setDirect(this->tabDirect, CONST_TAB);
   qDebug("In First Area");
}

void Calculate::inSecondArea(double x)
{
    int maxSpeed=steering->getMaxEngineSpeed();
    steering->setEngineL(abs(maxSpeed*x));
    steering->setEngineP(abs(maxSpeed*x));
    this->setDirectTab(0,0);
    this->setDirectTab(1,1);
    steering->setDirect(this->tabDirect, CONST_TAB);
    qDebug("In Second Area");
}

void Calculate::inThirdArea(double x)
{
    int maxSpeed=steering->getMaxEngineSpeed();
    steering->setEngineL(maxSpeed*x);
    steering->setEngineP(maxSpeed*x);
    this->setDirectTab(0,1);
    this->setDirectTab(1,0);
    steering->setDirect(this->tabDirect, CONST_TAB);
}

void Calculate::inFourthArea(double x, double y)
{
    int maxSpeed=steering->getMaxEngineSpeed();
    int valueL=maxSpeed*abs(y)-maxSpeed*abs(y)*abs(x);
    int valueP=maxSpeed*abs(y);
    steering->setEngineL(valueL);
    steering->setEngineP(valueP);
    this->setDirectTab(0,0);
    this->setDirectTab(1,0);
    steering->setDirect(this->tabDirect, CONST_TAB);
}

void Calculate::inFifthArea(double y)
{
    int maxSpeed=steering->getMaxEngineSpeed();
    int valueL=maxSpeed*abs(y);
    int valueP=maxSpeed*abs(y);
    steering->setEngineL(valueL);
    steering->setEngineP(valueP);
    this->setDirectTab(0,0);
    this->setDirectTab(1,0);
    steering->setDirect(this->tabDirect, CONST_TAB);
}

void Calculate::inSixthArea(double x, double y)
{
    int maxSpeed=steering->getMaxEngineSpeed();
    int valueP=maxSpeed*abs(y)-maxSpeed*abs(y)*abs(x);
    int valueL=maxSpeed*abs(y);
    steering->setEngineL(valueL);
    steering->setEngineP(valueP);
    this->setDirectTab(0,0);
    this->setDirectTab(1,0);
    steering->setDirect(this->tabDirect, CONST_TAB);
}

void Calculate::inSevenArea(double x, double y)
{
    int maxSpeed=steering->getMaxEngineSpeed();
    int valueL=maxSpeed*abs(y)-maxSpeed*abs(y)*abs(x);
    int valueP=maxSpeed*abs(y);
    steering->setEngineL(valueL);
    steering->setEngineP(valueP);
    this->setDirectTab(0,1);
    this->setDirectTab(1,1);
    steering->setDirect(this->tabDirect, CONST_TAB);
}

void Calculate::inEighthArea(double y)
{
    int maxSpeed=steering->getMaxEngineSpeed();
    int valueL=maxSpeed*abs(y);
    int valueP=maxSpeed*abs(y);
    steering->setEngineL(valueL);
    steering->setEngineP(valueP);
    this->setDirectTab(0,1);
    this->setDirectTab(1,1);
    steering->setDirect(this->tabDirect, CONST_TAB);
}

void Calculate::inNinthArea(double x, double y)
{
    int maxSpeed=steering->getMaxEngineSpeed();
    int valueP=maxSpeed*abs(y)-maxSpeed*abs(y)*abs(x);
    int valueL=maxSpeed*abs(y);
    steering->setEngineL(valueL);
    steering->setEngineP(valueP);
    this->setDirectTab(0,1);
    this->setDirectTab(1,1);
    steering->setDirect(this->tabDirect, CONST_TAB);
}

/*!
 * \brief Calculate::setDirectTab
 * Ustawianie danych w tablicy, czy lewa lub prawa strona (silnika), ma jechać do przodu czy do tyłu
 * \param where miejsce w tablicy
 * \param whatValue wartość w tablicy 0 - tył lub 1 - przód
 */
void Calculate::setDirectTab(int where, int whatValue)
{
    this->tabDirect[where]=whatValue;
}

/*!
 * \brief Calculate::~Calculate
 * Destruktor klasy Calculate
 */
Calculate::~Calculate()
{
   delete steering;
}
