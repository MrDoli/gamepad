#ifndef STEERING_H
#define STEERING_H

/*!
 * \brief The Steering class
 * W tej klasie są przechowywane dane do sterowania robotem
 * \param engineL - moc na lewy silnik
 * \param engineP - moc na prawy silnik
 * \param direct - tablica kierunku
 */
class Steering
{
private:
    int engineL;
    int engineP;
    /**
     * direct[0] - kierL
     * direct[1] - kierP
     * @brief direct
     */
    int direct[];
    const static int maxEngineSpeed=255;
public:
    Steering();
    void setEngineL(int value);
    int getEngineL();
    void setEngineP(int value);
    int getEngineP();
    void setDirect(int tab[],int size);
    int* getDirect();
    int getMaxEngineSpeed();
    ~Steering();
};

#endif // STEERING_H
