#include "mainwindow.h"
#include "servicegamepad.h"
#include <QApplication>
#include <QtCore/QCoreApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    ServiceGamepad *gamepad=new ServiceGamepad();
    gamepad->workGamepad();
    return a.exec();
}
