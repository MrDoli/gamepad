#ifndef ROBOT_H
#define ROBOT_H

/*!
 * \brief The Robot class
 * Stworzona klasa dla drugiej części zespołu, zajmująca się sprawami oprogramowania tylko na robocie.
 * Po prostu miejsce w którym, ewentualnie będzie można pisać komunikację i zarządzanie robotem, wykorzystując
 * parametry z innych klas.
 */
class Robot
{
public:
    Robot();
    void go();
    ~Robot();
};

#endif // ROBOT_H
