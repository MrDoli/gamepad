#include "steering.h"

#define STALA_TAB 2

/*!
 * \brief Steering::Steering
 * Konstruktor klasy Steering
 */
Steering::Steering()
{
    this->engineL=0;
    this->engineP=0;
    for(int i=0;i<STALA_TAB;i++) {
       this->direct[i]=0;
    }
}

void Steering::setEngineL(int value)
{
    this->engineL=value;
}

int Steering::getEngineL()
{
    return this->engineL;
}

void Steering::setEngineP(int value)
{
    this->engineP=value;
}

int Steering::getEngineP()
{
    return this->engineP;
}

void Steering::setDirect(int tab[],int size)
{
    for(int i=0;i<size;i++){
        this->direct[i]=tab[i];
    }
}

int* Steering::getDirect()
{
     return this->direct;
}

int Steering::getMaxEngineSpeed()
{
    return this->maxEngineSpeed;
}

Steering::~Steering()
{

}
