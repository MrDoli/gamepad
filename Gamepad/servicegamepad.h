#ifndef SERVICEGAMEPAD_H
#define SERVICEGAMEPAD_H

#include <QtCore/QObject>
#include <QtCore/QTimer>

#include "robot.h"
#include "steering.h"
#include "calculate.h"

QT_BEGIN_NAMESPACE
class QGamepad;
QT_END_NAMESPACE

/*!
 * \brief The ServiceGamepad class
 * Połączenie Gamepada z komuterem i sczytywanie danych z niego
 */
class ServiceGamepad:public QObject
{
    Q_OBJECT
public:
    explicit ServiceGamepad(QObject *parent=0);
    void workGamepad();
    void testGamepad();
    void setX_Axis(double value);
    void setY_Axis(double value);
    double getX_Axis();
    double getY_Axis();
    ~ServiceGamepad();
private:
    QGamepad *m_gamepad;
    Robot *robot;
    Calculate *calculate;
    const double border=0;
    double xAxis;
    double yAxis;
private slots:
    void frontBehind_slot(double);// go front or behind
    void leftRight_slot(double);
};

#endif // SERVICEGAMEPAD_H
