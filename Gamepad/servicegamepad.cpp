#include "servicegamepad.h"
#include "robot.h"
#include "steering.h"
#include "calculate.h"

#include <QtGamepad/QGamepad>
#include <QDebug>
#include <string>
#include <iostream>
#include <string.h>

/*!
 * @brief ServiceGamepad::ServiceGamepad
 * Konstruktor klasy ServiceGamepad - odpowiada za podłączenie gamepada do komputera.
 * Przydzielanie pamięci dla zmiennych m_gameapad i calculate
 */
ServiceGamepad::ServiceGamepad(QObject *parent): QObject(parent), m_gamepad(0)
{
    auto gamepads = QGamepadManager::instance()->connectedGamepads();
    if (gamepads.isEmpty()) {
        qDebug()<<"odlaczony";
        return;
    }

    m_gamepad = new QGamepad(*gamepads.begin(), this);
    robot=new Robot();
    calculate = new Calculate();
}

/*!
 * @brief ServiceGamepad::workGamepad
 * Funkcja uruchamiająca tryb pracy gamepada
 */
void ServiceGamepad::workGamepad()
{
    Robot robot;
    // FRONT, BEHIND
    connect(m_gamepad, &QGamepad::axisLeftYChanged, this, &ServiceGamepad::frontBehind_slot);// TAK OD QT 5.
    // LEFT, RIGHT
    connect(m_gamepad, &QGamepad::axisLeftXChanged, this, &ServiceGamepad::leftRight_slot);
}

/*!
 * @brief ServiceGamepad::leftRight_slot
 * Odbieranie sygnałów osi OX  Gamepada
 * Podejmowanie decyzji w którym z 8 obszarów znajduje się drążek, w zależności
 * od tego odpowienio ustawiane wartości sygnałów do silnika robota
 * @param value wartość wychylenia drążka
 */
void ServiceGamepad::leftRight_slot(double value)
{
    // GO TO LEFT
    if(value<border)
    {
        qDebug() << "-X- GO TO LEFT" << value;
        robot->go();
    }
    // GO TO RIGHT
    if(value>border)
    {
        qDebug() << "-X- GO TO RIGHT" << value;
    }
    setX_Axis(value);
    qDebug()<< "SERVICEGAMEPAD FROM GET Y IN X"<<this->getY_Axis();
    calculate->makeDesision(this->getX_Axis(),this->getY_Axis());
}

/*!
 * @brief ServiceGamepad::frontBehind_slot
 * Odbieranie sygnałów osi OY  Gamepada
 * Podejmowanie decyzji w którym z 8 obszarów znajduje się drążek, w zależności
 * od tego odpowienio ustawiane wartości sygnałów do silnika robota
 * @param value wartość wychylenia drążka
 */
void ServiceGamepad::frontBehind_slot(double value)
{
    // GO TO LEFT
    if(value<border)
    {
        qDebug() << "-Y- GO UP" << value;
    }
    // GO TO RIGHT
    if(value>border)
    {
        qDebug() << "-Y- GO DOWN" << value;

    }
    setY_Axis(value);
    qDebug()<< "SERVICEGAMEPAD FROM GET X IN Y"<<this->getX_Axis();
    calculate->makeDesision(this->getX_Axis(),this->getY_Axis());
}

/*!
 * @brief ServiceGamepad::testGamepad
 * Testowanie ustawień Gamepada
 */
void ServiceGamepad::testGamepad()
{
    // TO DOSTOSOWAC DO JOYSTICKA
    qDebug()<<"TEST KLAWISZY I FUNKCJI";
    connect(m_gamepad, &QGamepad::axisLeftXChanged, this, [](double value){
        qDebug() << "Left X" << value;  // oś OX -1 do 1 (-1 lewa, 1 prawa)
    });
    connect(m_gamepad, &QGamepad::axisLeftYChanged, this, [](double value){
        qDebug() << "Left Y" << value; // oś OY -1 do 1 (-1 góra, 1 dół)
    });
    connect(m_gamepad, &QGamepad::axisRightXChanged, this, [](double value){
        qDebug() << "Right X" << value;
    });
    connect(m_gamepad, &QGamepad::axisRightYChanged, this, [](double value){
        qDebug() << "Right Y" << value;
    });
    connect(m_gamepad, &QGamepad::buttonAChanged, this, [](bool pressed){
        qDebug() << "Button A" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonBChanged, this, [](bool pressed){
        qDebug() << "Button B" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonXChanged, this, [](double pressed){
        qDebug() << "Button X" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonYChanged, this, [](bool pressed){
        qDebug() << "Button Y" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonL1Changed, this, [](bool pressed){
        qDebug() << "Button L1" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonR1Changed, this, [](bool pressed){
        qDebug() << "Button R1" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonL2Changed, this, [](double value){
        qDebug() << "Button L2: " << value;
    });
    connect(m_gamepad, &QGamepad::buttonR2Changed, this, [](double value){
        qDebug() << "Button R2: " << value;
    });
    connect(m_gamepad, &QGamepad::buttonSelectChanged, this, [](bool pressed){
        qDebug() << "Button Select" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonStartChanged, this, [](bool pressed){
        qDebug() << "Button Start" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonGuideChanged, this, [](bool pressed){
        qDebug() << "Button Guide" << pressed;
    });
}

void ServiceGamepad::setX_Axis(double value)
{
    this->xAxis=value;
}

void ServiceGamepad::setY_Axis(double value)
{
    this->yAxis=value;
}

double ServiceGamepad::getX_Axis()
{
    return this->xAxis;
}

double ServiceGamepad::getY_Axis()
{
    return this->yAxis;
}

/**
 * @brief ServiceGamepad::~ServiceGamepad
 * Destruktor klasy ServiceGamepad
 */
ServiceGamepad::~ServiceGamepad()
{
    delete robot;
    delete calculate;
    delete m_gamepad;
}
