#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "servicegamepad.h"

namespace Ui {
class MainWindow;
}

/*!
 * \brief The MainWindow class
 * Klasa pozwalająca wyświetlić okienko, jest to wymagane aby w niektórych przypadkach QT wykryło pada,
 * problem znany i opisany na forach w tematyce QT.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
