#ifndef CALCULATE_H
#define CALCULATE_H

#include "steering.h"

/*!
 * \brief The Calculate class
 * Klasa odpowiadająca za obliczanie wartości wysyłanych do silnika
 * Funkcje in(liczba)Area - ustawiają dane do silników w zależności
 * od tego w którym obszarze znajdował sie drążek po wychyleniu
 */
class Calculate
{
private:
    Steering* steering;
    int tabDirect[2];       //tu dodałem
public:
    Calculate();
    void makeDesision(double x, double y);
    void inFirstArea();
    void inSecondArea(double x);
    void inThirdArea(double x);
    void inFourthArea(double x, double y);
    void inFifthArea(double y);
    void inSixthArea(double x, double y);
    void inSevenArea(double x, double y);
    void inEighthArea(double y);
    void inNinthArea(double x, double y);
    void setDirectTab(int where, int whatValue);
    ~Calculate();
};

#endif // CALCULATE_H
